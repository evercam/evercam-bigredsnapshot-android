package io.evercam.bigredsnapshot;

public class UrlStatus
{
	public final static String LOCAL = "Local";
	public final static String REMOTE = "Remote";
	public final static String EVERCAM = "Evercam";
	public final static String LATEST = "Latest";
}
