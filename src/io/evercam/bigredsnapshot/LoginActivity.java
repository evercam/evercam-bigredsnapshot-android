package io.evercam.bigredsnapshot;

import com.bugsense.trace.BugSenseHandler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import io.evercam.*;
import io.evercam.bigredsnapshot.helper.CustomedDialog;
import io.evercam.bigredsnapshot.helper.PrefsManager;
import io.evercam.bigredsnapshot.helper.PropertyReader;
import io.evercam.bigredsnapshot.tasks.CheckInternetTask;

public class LoginActivity extends Activity
{
	private UserLoginTask loginTask = null;

	private String username;
	private String password;
	private SharedPreferences sharedPrefs;

	private EditText usernameView;
	private EditText passwordView;
	private View loginFormView;
	private View loginStatusView;
	private TextView loginStatusMessageView;
	private TextView signUpLink;
	private PropertyReader propertyReader;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		/* BugSense */
		propertyReader = new PropertyReader(getApplicationContext());
		if (propertyReader.isPropertyExist(PropertyReader.KEY_BUG_SENSE))
		{
			String bugSenseCode = propertyReader.getPropertyStr(PropertyReader.KEY_BUG_SENSE);
			BugSenseHandler.initAndStartSession(LoginActivity.this, bugSenseCode);
		}

		new CheckInternetTaskLogin(LoginActivity.this).execute();
	}

	@Override
	protected void onStart()
	{
		super.onStart();

		if (propertyReader.isPropertyExist(PropertyReader.KEY_BUG_SENSE))
		{
			BugSenseHandler.startSession(this);
		}
	}

	@Override
	protected void onStop()
	{
		super.onStop();

		if (propertyReader.isPropertyExist(PropertyReader.KEY_BUG_SENSE))
		{
			BugSenseHandler.closeSession(this);
		}
	}

	@Override
	protected void onRestart()
	{
		super.onRestart();
		new CheckInternetTaskLogin(LoginActivity.this).execute();
	}

	private void setUpPage()
	{
		setContentView(R.layout.activity_login);
		if (isUserLogged(sharedPrefs))
		{
			startChooseCamera();
		}
		else
		{
			BigRedSnapshot.sendScreenAnalytics(this, getString(R.string.screen_login));
		}

		setUnderLine();
		usernameView = (EditText) findViewById(R.id.email);
		passwordView = (EditText) findViewById(R.id.password);
		passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener(){
			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent)
			{
				if (id == R.id.login || id == EditorInfo.IME_NULL)
				{
					attemptLogin();
					return true;
				}
				return false;
			}
		});

		loginFormView = findViewById(R.id.login_form);
		loginStatusView = findViewById(R.id.login_status);
		loginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View view)
			{
				attemptLogin();
			}
		});

		signUpLink.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v)
			{
				Intent signupIntent = new Intent();
				signupIntent.setClass(LoginActivity.this, SignUpActivity.class);
				startActivity(signupIntent);
			}
		});
	}

	public void attemptLogin()
	{
		if (loginTask != null)
		{
			return;
		}

		usernameView.setError(null);
		passwordView.setError(null);

		username = usernameView.getText().toString();
		password = passwordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		if (TextUtils.isEmpty(password))
		{
			passwordView.setError(getString(R.string.error_field_required));
			focusView = passwordView;
			cancel = true;
		}
		else if (password.contains(" "))
		{
			passwordView.setError(getString(R.string.invalid_password));
			focusView = passwordView;
			cancel = true;
		}

		if (TextUtils.isEmpty(username))
		{
			usernameView.setError(getString(R.string.error_field_required));
			focusView = usernameView;
			cancel = true;
		}
		else if (username.contains(" "))
		{
			usernameView.setError(getString(R.string.invalid_username));
			focusView = usernameView;
			cancel = true;
		}
		else if (username.length() < 3)
		{
			usernameView.setError(getString(R.string.username_too_short));
			focusView = usernameView;
			cancel = true;
		}

		if (cancel)
		{
			focusView.requestFocus();
		}
		else
		{
			loginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			loginTask = new UserLoginTask();
			loginTask.execute((Void) null);
		}
	}

	private void showProgress(final boolean show)
	{
		loginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
		loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
	}

	public class UserLoginTask extends AsyncTask<Void, Void, Boolean>
	{
		String errorMessage = "UserLoginTaskError";

		@Override
		protected Boolean doInBackground(Void... params)
		{
			try
			{
				ApiKeyPair userKeyPair = API.requestUserKeyPairFromEvercam(username, password);
				String userApiKey = userKeyPair.getApiKey();
				String userApiId = userKeyPair.getApiId();
				PrefsManager.saveEvercamUserKeyPair(sharedPrefs, userApiKey, userApiId);
				API.setUserKeyPair(userApiKey, userApiId);
				User evercamUser = new User(username);
				PrefsManager.saveEvercamCredential(sharedPrefs, evercamUser, password);
				return true;
			}
			catch (EvercamException e)
			{
				errorMessage = e.getMessage();
			}
			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success)
		{
			loginTask = null;
			showProgress(false);

			if (success)
			{
				startChooseCamera();
			}
			else
			{
				Toast toast = Toast.makeText(getApplicationContext(), errorMessage,
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				passwordView.setText(null);
			}
		}

		@Override
		protected void onCancelled()
		{
			loginTask = null;
			showProgress(false);
		}
	}

	public static boolean isUserLogged(SharedPreferences sharedPrefs)
	{
		String savedUsername = sharedPrefs.getString(PrefsManager.KEY_USER_NAME, null);
		if (savedUsername != null)
		{
			return true;
		}
		return false;
	}

	private void startChooseCamera()
	{
		Intent intentMain = new Intent();
		intentMain.setClass(LoginActivity.this, ChooseCameraActivity.class);
		startActivity(intentMain);
	}

	@Override
	public void onBackPressed()
	{
		Intent backIntent = new Intent();
		backIntent.setClass(LoginActivity.this, SlideActivity.class);
		startActivity(backIntent);
	}

	private void setUnderLine()
	{
		signUpLink = (TextView) findViewById(R.id.signupLink);
		SpannableString spanString = new SpannableString(this.getResources().getString(
				R.string.create_account));
		spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
		signUpLink.setText(spanString);
	}

	private class CheckInternetTaskLogin extends CheckInternetTask
	{
		public CheckInternetTaskLogin(Context context)
		{
			super(context);
		}

		@Override
		protected void onPostExecute(Boolean hasNetwork)
		{
			if (hasNetwork)
			{
				setUpPage();
			}
			else
			{
				CustomedDialog.getNoInternetDialog(LoginActivity.this).show();
			}
		}
	}
}
