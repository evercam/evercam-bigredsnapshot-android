package io.evercam.bigredsnapshot;

import io.evercam.bigredsnapshot.helper.PrefsManager;
import io.evercam.bigredsnapshot.helper.PropertyReader;
import io.evercam.bigredsnapshot.tasks.CaptureSnapshotTask;

import com.bugsense.trace.BugSenseHandler;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

public class BigButtonActivity extends Activity
{
	private final String TAG = "bigredsnapshot-BigButtonActivity";
	private ImageView redButtonImgView;
	public ImageView snapshotImgView;
	private MediaPlayer clickPlayer;
	private MediaPlayer releasePlayer;
	private SharedPreferences sharedPrefs;
	private String cameraId;
	private String url;
	private PropertyReader propertyReader;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_big_button);
		Bundle extras = getIntent().getExtras();
		cameraId = extras.getString("cameraID");
		url = extras.getString("url");
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(BigButtonActivity.this);

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);

		// Google Analytics Screen Report
		BigRedSnapshot.sendScreenAnalytics(this, getString(R.string.screen_big_button));

		redButtonImgView = (ImageView) findViewById(R.id.bigbutton_imgview);
		snapshotImgView = (ImageView) findViewById(R.id.snapshot_imgview);
		redButtonImgView.setOnTouchListener(new OnTouchListener(){

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				switch (event.getAction() & MotionEvent.ACTION_MASK)
				{

				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_POINTER_DOWN:
					redButtonImgView.setImageResource(R.drawable.button2);
					if (clickPlayer != null)
					{
						clickPlayer.start();
					}
					return true;

				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_POINTER_UP:
					redButtonImgView.setImageResource(R.drawable.button1);
					if (releasePlayer != null)
					{
						releasePlayer.start();
					}
					BigRedSnapshot.sendEventAnalytics(BigButtonActivity.this,
							R.string.category_big_button_clicks, R.string.action_click_button,
							R.string.label_click_button);
					startCaptureTask();
					return true;
				}
				return false;
			}
		});

		snapshotImgView.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v)
			{
				BigRedSnapshot.sendEventAnalytics(BigButtonActivity.this,
						R.string.category_big_button_clicks, R.string.action_view_images,
						R.string.label_view_from_image);
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("content://media/internal/images/media"));
				startActivity(intent);
			}
		});
		new AsyncTask<Void, Void, Void>(){
			@Override
			protected Void doInBackground(Void... params)
			{
				clickPlayer = MediaPlayer.create(BigButtonActivity.this, R.raw.click);
				releasePlayer = MediaPlayer.create(BigButtonActivity.this, R.raw.release);
				return null;
			}
		}.execute();

		/* BugSense */
		propertyReader = new PropertyReader(getApplicationContext());
		if (propertyReader.isPropertyExist(PropertyReader.KEY_BUG_SENSE))
		{
			String bugSenseCode = propertyReader.getPropertyStr(PropertyReader.KEY_BUG_SENSE);
			BugSenseHandler.initAndStartSession(BigButtonActivity.this, bugSenseCode);
		}

		startCaptureTask();
	}

	@Override
	protected void onStart()
	{
		super.onStart();

		if (propertyReader.isPropertyExist(PropertyReader.KEY_BUG_SENSE))
		{
			BugSenseHandler.startSession(this);
		}
	}

	@Override
	protected void onStop()
	{
		super.onStop();

		if (propertyReader.isPropertyExist(PropertyReader.KEY_BUG_SENSE))
		{
			BugSenseHandler.closeSession(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.big_button, menu);

		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item)
	{
		if (item.getItemId() == R.id.action_viewSnapshot)
		{
			BigRedSnapshot.sendEventAnalytics(BigButtonActivity.this,
					R.string.category_big_button_clicks, R.string.action_view_images,
					R.string.label_view_from_menu);
			Intent intent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("content://media/internal/images/media"));
			startActivity(intent);
		}
		else if (item.getItemId() == R.id.action_logOut)
		{
			showConfirmLogout();
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public Intent getParentActivityIntent()
	{
		this.finish();
		return super.getParentActivityIntent();
	}

	private void launchLogOut()
	{
		PrefsManager.clearEvercamCredential(sharedPrefs);

		Intent intentLogin = new Intent();
		intentLogin.setClass(BigButtonActivity.this, LoginActivity.class);
		startActivity(intentLogin);
	}

	private void showConfirmLogout()
	{
		AlertDialog comfirmLogoutDialog = new AlertDialog.Builder(BigButtonActivity.this)

		.setMessage(R.string.comfirmLogOutMsg)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						BigRedSnapshot.sendEventAnalytics(BigButtonActivity.this,
								R.string.category_big_button_clicks, R.string.action_logout,
								R.string.label_logout_from_button);

						launchLogOut();
					}
				}).setNegativeButton(R.string.no, new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						return;
					}
				}).create();
		comfirmLogoutDialog.show();
	}

	private void startCaptureTask()
	{
		CaptureSnapshotTask captureSnapshotTask = new CaptureSnapshotTask(cameraId, url,
				BigButtonActivity.this);
		captureSnapshotTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
}
