package io.evercam.bigredsnapshot.helper;

import io.evercam.EvercamException;
import io.evercam.User;
import android.content.SharedPreferences;

public class PrefsManager
{
	public final static String KEY_USER_NAME = "userName";
	public final static String KEY_PASSWORD = "passWord";
	public final static String KEY_EMAIL = "email";
	public final static String KEY_FORENAME = "forename";
	public final static String KEY_LASTNAME = "lastname";
	public final static String KEY_COUNTRY = "country";
	public final static String KEY_USER_API_KEY = "userApiKey";
	public final static String KEY_USER_API_ID = "userApiId";

	private PrefsManager()
	{
	}

	public static String getUsername(SharedPreferences sharedPrefs)
	{
		return sharedPrefs.getString(KEY_USER_NAME, null);
	}

	public static String getPassword(SharedPreferences sharedPrefs)
	{
		return sharedPrefs.getString(KEY_PASSWORD, null);
	}

	public static void saveEvercamCredential(SharedPreferences sharedPrefs, User user,
			String password) throws EvercamException
	{
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putString(KEY_USER_NAME, user.getId());
		editor.putString(KEY_PASSWORD, password);
		editor.putString(KEY_COUNTRY, user.getCountry());
		editor.putString(KEY_EMAIL, user.getEmail());
		editor.putString(KEY_FORENAME, user.getFirstName());
		editor.putString(KEY_LASTNAME, user.getLastName());
		editor.commit();
	}

	public static void saveEvercamUserKeyPair(SharedPreferences sharedPrefs, String apiKey,
			String apiId)
	{
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putString(KEY_USER_API_KEY, apiKey);
		editor.putString(KEY_USER_API_ID, apiId);
		editor.commit();
	}

	public static String getUserApiKey(SharedPreferences sharedPrefs)
	{
		return sharedPrefs.getString(KEY_USER_API_KEY, null);
	}

	public static String getUserApiId(SharedPreferences sharedPrefs)
	{
		return sharedPrefs.getString(KEY_USER_API_ID, null);
	}

	public static void clearEvercamCredential(SharedPreferences sharedPrefs)
	{
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putString(KEY_USER_NAME, null);
		editor.putString(KEY_PASSWORD, null);
		editor.putString(KEY_COUNTRY, null);
		editor.putString(KEY_EMAIL, null);
		editor.putString(KEY_FORENAME, null);
		editor.putString(KEY_LASTNAME, null);
		editor.commit();
	}
}
