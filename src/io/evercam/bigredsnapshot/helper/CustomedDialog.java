package io.evercam.bigredsnapshot.helper;

import io.evercam.bigredsnapshot.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

public class CustomedDialog
{
	// Alert dialog with single button
	public static AlertDialog getAlertDialog(Context context, String title, String message)
	{
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		dialogBuilder.setTitle(title);
		dialogBuilder.setMessage(message);
		dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = dialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		return alertDialog;
	}

	// Dialog that shows when Internet is not connected.
	public static AlertDialog getNoInternetDialog(final Context context)
	{
		AlertDialog.Builder connectDialogBuilder = new AlertDialog.Builder(context);
		connectDialogBuilder.setMessage(R.string.noNetworkMsg);

		connectDialogBuilder.setPositiveButton(R.string.settings,
				new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
					}
				});
		connectDialogBuilder.setNegativeButton(R.string.notNow,
				new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						return;
					}
				});
		connectDialogBuilder.setTitle(R.string.noNetwork);
		connectDialogBuilder.setCancelable(false);
		AlertDialog alertDialog = connectDialogBuilder.create();
		return alertDialog;
	}
}
