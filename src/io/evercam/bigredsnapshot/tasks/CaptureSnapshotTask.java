package io.evercam.bigredsnapshot.tasks;

import io.evercam.Camera;
import io.evercam.EvercamException;
import io.evercam.Snapshot;
import io.evercam.bigredsnapshot.BigButtonActivity;
import io.evercam.bigredsnapshot.R;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.bugsense.trace.BugSenseHandler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class CaptureSnapshotTask extends AsyncTask<Void, Bitmap, Boolean>
{
	private final String TAG = "bIgredsnapshoT-CaptureSnapshotTask";
	private BigButtonActivity bigButtonActivity;
	private String cameraId;
	private String url;
	private int count;
	private final String SNAPSHOT_FOLDER_NAME = "BigRedSnapshot";
	private final String ARCHIVE_NOTE = "Big Red Snapshot auto save";
	private Toast savingToast;
	private Toast notAvailableToast;
	private Runnable imgRunnable;

	public CaptureSnapshotTask(String cameraId, String url, BigButtonActivity bigButtonActivity)
	{
		this.url = url;
		this.cameraId = cameraId;
		this.bigButtonActivity = bigButtonActivity;
	}

	@Override
	protected Boolean doInBackground(Void... arg0)
	{
		Bitmap snapshotBitmap = getSnapshotBit(cameraId);
		if (snapshotBitmap != null)
		{
			publishProgress(snapshotBitmap);
			capture(snapshotBitmap);
			updateGallery();
			saveSnapshotOnline();
			return true;
		}
		return false;
	}

	@Override
	protected void onPreExecute()
	{
		imgRunnable = new Runnable(){
			@Override
			public void run()
			{
				bigButtonActivity.snapshotImgView
						.setBackgroundResource(R.drawable.ingview_border_null);
			}
		};
		savingToast = Toast.makeText(bigButtonActivity, R.string.saving, Toast.LENGTH_SHORT);
		savingToast.show();
	}

	@Override
	protected void onPostExecute(Boolean success)
	{
		if (!success)
		{
			notAvailableToast = Toast.makeText(bigButtonActivity, R.string.camera_offline,
					Toast.LENGTH_SHORT);
			notAvailableToast.show();
		}
	}

	@Override
	protected void onProgressUpdate(Bitmap... bitmap)
	{
		bigButtonActivity.snapshotImgView.setBackgroundResource(R.drawable.imgview_border);
		bigButtonActivity.snapshotImgView.setVisibility(View.VISIBLE);
		bigButtonActivity.snapshotImgView.setImageBitmap(bitmap[0]);
		bigButtonActivity.snapshotImgView.postDelayed(imgRunnable, 1500);
	}

	public Bitmap getSnapshotBit(String cameraId)
	{
		Bitmap bitmap = null;

		try
		{
			InputStream inputStream;
			Camera camera = Camera.getById(cameraId,false);
			if (url == null || url.isEmpty())
			{
				inputStream = camera.getSnapshotFromEvercam();
			}
			else
			{
				inputStream = Camera.getStreamFromUrl(url, camera.getUsername(),
						camera.getPassword());
			}
			bitmap = BitmapFactory.decodeStream(inputStream);
		}
		catch (EvercamException e)
		{
			Log.e(TAG, e.toString());
		}
		catch (Exception e)
		{
			// OutOfMemoryException could occur here.
			BugSenseHandler.sendException(e);
			Log.e(TAG, e.toString());
		}

		return bitmap;
	}

	public void capture(Bitmap snapshotBitmap)
	{
		File folder = new File(
				Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
						+ File.separator + SNAPSHOT_FOLDER_NAME);
		if (!folder.exists())
		{
			folder.mkdirs();
		}

		if (snapshotBitmap != null)
		{
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			snapshotBitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

			int i = 0;
			String filename = "snapshot" + Integer.toString(i) + ".jpg";

			File f = new File(folder.getPath() + File.separator + filename);
			while (f.exists())
			{
				i++;
				filename = "snapshot" + Integer.toString(i) + ".jpg";
				f = new File(folder.getPath() + File.separator + filename);
			}

			count = i;

			try
			{
				f.createNewFile();
				FileOutputStream fo = new FileOutputStream(f);
				fo.write(bytes.toByteArray());
				fo.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void updateGallery()
	{
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
		{
			bigButtonActivity
					.sendBroadcast(new Intent(
							Intent.ACTION_MEDIA_MOUNTED,
							Uri.parse("file://"
									+ Environment
											.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES))));
		}
		else
		{
			new SingleMediaScanner(bigButtonActivity,
					Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
							+ File.separator + SNAPSHOT_FOLDER_NAME + "/snapshot" + count + ".jpg");
		}
	}

	public void saveSnapshotOnline()
	{
		try
		{
			Snapshot.store(cameraId, ARCHIVE_NOTE);
		}
		catch (EvercamException e)
		{
			Log.e("bigredsnapshot", e.getMessage());
		}
		catch (Exception e)
		{
			BugSenseHandler.sendException(e);
		}
	}

	class SingleMediaScanner implements MediaScannerConnectionClient
	{
		MediaScannerConnection connection;
		Context ctxt;
		private String imagepath;

		public SingleMediaScanner(Context ctxt, String url)
		{
			this.ctxt = ctxt;
			startScan(url);
		}

		public void startScan(String url)
		{
			imagepath = url;
			if (connection != null) connection.disconnect();
			connection = new MediaScannerConnection(ctxt, this);
			connection.connect();
		}

		@Override
		public void onMediaScannerConnected()
		{
			try
			{
				connection.scanFile(imagepath, null);
			}
			catch (java.lang.IllegalStateException e)
			{
			}
		}

		@Override
		public void onScanCompleted(String path, Uri uri)
		{
			connection.disconnect();
		}
	}
}
