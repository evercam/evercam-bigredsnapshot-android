package io.evercam.bigredsnapshot;

import io.evercam.Camera;
import io.evercam.EvercamException;
import io.evercam.Snapshot;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class SnapshotRequest
{
	public static final String TAG = "bigredsnapshot-SnapshotRequest";

	public static Bitmap getExternalSnapshotBit(Camera camera)
	{
		Bitmap bitmap = null;
		try
		{
			InputStream inputStream = Camera.getStreamFromUrl(camera.getExternalJpgUrl(),
					camera.getUsername(), camera.getPassword());
			bitmap = BitmapFactory.decodeStream(inputStream);
		}
		catch (Exception e)
		{
			Log.e(TAG, e.toString());
		}
		return bitmap;
	}

	public static Bitmap getInternalSnapshotBit(Camera camera)
	{
		Bitmap bitmap = null;
		try
		{
			InputStream inputStream = Camera.getStreamFromUrl(camera.getInternalJpgUrl(),
					camera.getUsername(), camera.getPassword());
			bitmap = BitmapFactory.decodeStream(inputStream);
		}
		catch (Exception e)
		{
			Log.e(TAG, e.toString());
		}
		return bitmap;
	}

	public static Bitmap getSnapshotBitFromEvercam(Camera camera)
	{
		Bitmap bitmap = null;
		try
		{
			InputStream inputStream = camera.getSnapshotFromEvercam();
			bitmap = BitmapFactory.decodeStream(inputStream);
		}
		catch (Exception e)
		{
			Log.e(TAG, e.toString());
		}
		return bitmap;
	}

	public static Bitmap getLatestSnapshot(Camera camera) throws EvercamException, Exception
	{
		Snapshot latestSnapshot = Snapshot.getLatestArchivedSnapshot(camera.getId(), true);
		byte[] snapshotByte = latestSnapshot.getData();
		Bitmap bitmap = BitmapFactory.decodeByteArray(snapshotByte, 0, snapshotByte.length);
		return bitmap;
	}

}
