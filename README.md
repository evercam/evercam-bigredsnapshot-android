# Evercam Big Red Snapshot

Big Red Button Snapshot is a sample to show a simple use case of how an android app interacting with the Evercam API.

**Note:** This repo is no longer active.

## Features

* Sometimes there’s no better interface than a nice Big Red Button
* Sign in with Evercam or create an Evercam user using [Evercam API - Users](http://www.evercam.io/docs/api/v1/swagger#!/users)
* Save camera live snapshot when you press the button and save it to your local storage. 
* Fetching snapshot using [Evercam API - Cameras](http://www.evercam.io/docs/api/v1/swagger#!/cameras)
* An example to use [Java Wrapper around Evercam API](https://github.com/evercam/evercam.java)

## Published App
The app is now unpublished from App Store, if you've already installed it, you might still have access to the link:
<a href="https://play.google.com/store/apps/details?id=io.evercam.bigredsnapshot&hl=en"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/apps/en-play-badge-border.png" width="200"/></a>

## Build

    # Checkout from Git
    git clone https://github.com/evercam/android.bigredsnapshot.git
